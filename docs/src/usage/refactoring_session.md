# Refactoring session

Install [`watchexec`](https://watchexec.github.io/), then :

```sh
watchexec -- git gamble --refactor
```

This is just [TCR](../theory.md#tcr) (without [TDD](../theory.md#tdd))
