# Usage examples

There is several examples with several languages in [the nix-sandboxes repository](https://pinage404.gitlab.io/nix-sandboxes/)

In each language folder, there is a `.envrc` file, which contains the variable `GAMBLE_TEST_COMMAND`, that mainly refers to [`mask` commands](https://github.com/jacobdeichert/mask/) that are described in the sibling file `maskfile.md`
