---
status: "accepted"
date: 2023-11-11
deciders: pinage404
---

# Binaries organisation

## Story

We (a colleague [referred to as "he" in the following text] and pinage404) used `git-gamble` at work

He was not used to using TDD nor TCR

The fear of losing code led to an anti-pattern :
before gambling,
he took a lot of time to read the code carefully,
compile it and execute it in his head,
which slowed down the group

This strong methodology should lead you to let yourself be carried along by the tools

He recommended limiting the duration of iterations

---

`git-gamble` has been used by several groups and pinage404 has seen this anti-pattern several times

## Context and Problem Statement

To solve the problem of iteration duration, [another tool has been created since 2022-07-11](https://gitlab.com/pinage404/git-gamble/-/commit/acd29d6eced442c439a3bdaf28cabc53665702c2), but it is neither documented nor easily distributed

[The first version of `git-time-keeper` was written in Bash](https://gitlab.com/pinage404/git-gamble/-/blob/18dbcffc60207590713d0966ec4590db1a00291f/script/git-time-keeper), and works _most of the time_, in order to have a more stable experience, it will be rewritten in Rust

`git-gamble` and `git-time-keeper` are tools that work independently and can be used together for an optimal experience

How to create, maintain and distribute several tools ?

## Decision Drivers

* from the maintainer's point of view
  * easy to set up
  * easy to maintain
  * easy to distribute
  * avoid duplication of configuration and utilities
* from the user's point of view
  * easy to install
  * easy to use
  * easy to understand that each tool can be used separately
  * easy to use tools together for an optimal experience

## Considered Options

* **Repos** : several independent repositories
* **Workspaces** : 1 repository with several Cargo workspaces
* **Binaries** : 1 repository with 1 crate containing several binaries

## Decision Outcome

Chosen option: "Binaries", because this solution seems to have the fewest downsides, see the table below

## Pros and Cons of the Options

|                                  | **Repos**                                   | **Workspaces**                           | **Binaries**                                                               |
| :------------------------------- | :------------------------------------------ | :--------------------------------------- | :------------------------------------------------------------------------- |
| **Maintainer**                   |                                             |                                          |                                                                            |
| easy to set up                   | **Good**, easiest, just `git clone`         | **Bad**, need a little of work           | **Neutral**, Good if every tools support it                                |
| easy to maintain                 | **Bad**, need several maitainance           | **Good**, that's what workspaces are for | **Neutral**, easy but risk of confusion between what belongs to which tool |
| easy to distribute               | **Bad**, need to resetup external platforms | **Neutral** if every tools support it    | **Neutral**, Good if every tools support it                                |
| avoid duplication                | **Bad**                                     | **Neutral**, can have a shared Crate     | **Good**                                                                   |
| **User**                         |                                             |                                          |                                                                            |
| easy to install                  | **Bad**, need several installations         | **Neutral**                              | **Neutral**, Good if every tools support it                                |
| easy to use                      | **Neutral**                                 | **Neutral**                              | **Neutral**                                                                |
| understand tools are independent | **Good**                                    | **Neutral**                              | **Neutral**                                                                |
| easy to use tools together       | **Bad**                                     | **Neutral**                              | **Neutral**                                                                |
| **Total**                        | Good 2 Neutral 1 Bad 5 = -3                 | Good 1 Neutral 6 Bad 1 = 0               | Good 1 Neutral 7 Bad 0 = +1                                                |
