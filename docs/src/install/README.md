# Installation

{{#include ./_repology.md}}

There is several methods to install depending on your operating system:

- [Nix (Mac / Linux / Windows)](./methods/nix.md)
- [Debian](./methods/debian.md)
- [Homebrew (Mac OS X)](./methods/homebrew.md)
- [Chocolatey (Windows)](./methods/chocolatey.md)
- [Cargo](./methods/cargo.md)
- [Download the binary](./methods/binary.md)

{{#include ./_please_improve_install.md}}
