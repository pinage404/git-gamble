# Download the binary

[![Release](https://img.shields.io/gitlab/v/release/pinage404/git-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/releases)

{{#include ../_requirements.md}}

## Install

Only for Linux and Windows `x86_64`

1. Download the binary on the [release page](https://gitlab.com/pinage404/git-gamble/-/releases/version%2F2.9.0)
1. Rename it to `git-gamble`
1. [Put it in your `$PATH`](https://superuser.com/a/284361)

Note: the latest version, not yet released, is also available

* [Linux](https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/target%2Frelease%2Fgit-gamble?job=build%20release%20for%20Linux)
* [Windows](https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/target%2Fx86_64-pc-windows-gnu%2Frelease%2Fgit-gamble.exe?job=build%20release%20for%20Windows)

{{#include ../_check_the_installation.md}}

## Update

There is no update mechanism

## Uninstall

Just remove the binary from [your `$PATH`](https://superuser.com/a/284361)
