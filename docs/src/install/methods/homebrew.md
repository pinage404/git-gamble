# Install using Homebrew on Mac OS X

[![Homebrew available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Homebrew&prefix=v&logo=homebrew)](https://gitlab.com/pinage404/git-gamble/-/packages)

## Requirements

1. [Install Homebrew](https://brew.sh/)
1. Check the installation with this command

   ```sh
   brew --version
   ```

   If it has been **well settled**, it should output something like this :

   ```txt
   Homebrew 4.3.14
   Homebrew/homebrew-core (git revision 7a574d89134; last commit 2024-08-07)
   ```

   Else if it has been **badly settled**, it should output something like this :

   ```txt
   brew: command not found
   ```

## Install

Run these commands

```sh
brew tap pinage404/git-gamble https://gitlab.com/pinage404/git-gamble.git
brew install --HEAD git-gamble
```

{{#include ../_check_the_installation.md}}

## Upgrade

`git-gamble` has not yet been packaged by Homebrew

To upgrade `git-gamble`, run this command

```sh
brew reinstall git-gamble
```

## Uninstall

```sh
brew uninstall git-gamble
brew untap pinage404/git-gamble
```

{{#include ../_please_improve_install.md}}

[Homebrew requires repositories to have at least 75 stars and 30 forks](https://github.com/Homebrew/brew/blob/92ddc02005e1f1a64cdf754ddc887ad3f921dce2/Library/Homebrew/utils/shared_audits.rb#L148)

<!-- markdownlint-disable MD039 MD045 -->
So, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/git-gamble?style=social)](https://gitlab.com/pinage404/git-gamble)
and [create a fork ![](https://img.shields.io/gitlab/forks/pinage404%2Fgit-gamble)](https://gitlab.com/pinage404/git-gamble/-/forks) for any [contribution](https://git-gamble.is-cool.dev/contributing/index.html)
<!-- markdownlint-restore -->
