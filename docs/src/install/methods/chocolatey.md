# Install using Chocolatey on Windows

[![Chocolatey available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Chocolatey&prefix=v&logo=chocolatey)](https://gitlab.com/pinage404/git-gamble/-/packages)

## Requirements

### Chocolatey

1. [Install Chocolatey](https://chocolatey.org/install)
1. Check the installation with this command

   ```sh
   choco --version
   ```

   If it has been **well settled**, it should output something like this :

   ```txt
   2.3.0
   ```

   Else if it has been **badly settled**, it should output something like this :

   ```txt
   choco: command not found
   ```

### Git

{{#include ../_requirements_git.md}}

## Install

Note:
If you installed Chocolatey from an administrative shell,
make sure to launch all `choco` commands from an administrative shell as well.

Check the version of Chocolatey installed using this command

```sh
choco --version
```

* Chocolatey v2.x

  ```shell
  choco source add --name=git-gamble --source="https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/nuget/index.json"
  choco install git-gamble.portable
  ```

* Chocolatey v1.x

  Use the Nuget v2 source :

  ```shell
  choco source add --name=git-gamble --source="https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/nuget/v2"
  choco install git-gamble.portable --version=2.9.0
  ```

  The Gitlab registry doesn't seem to fully support package discovery on the v2 source,
  so the version needs to be specified explicitly. [This issue](https://gitlab.com/gitlab-org/gitlab/-/issues/416406) might address this.

Note: [the latest version, not yet released, is also available](https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/packaging%2FChocolatey%2Fgit-gamble.portable.2.9.0.nupkg?job=package%20Chocolatey)

{{#include ../_check_the_installation.md}}

## Update

```sh
choco upgrade git-gamble.portable
```

With Chocolatey v1.x and/or the Nuget v2 source :

```sh
choco upgrade git-gamble.portable --version=2.9.0
```

## Uninstall

```sh
choco uninstall git-gamble.portable
```

You might also want to remove the source :

```sh
choco source remove --name=git-gamble
```

{{#include ../_please_improve_install.md}}
