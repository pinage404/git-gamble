# `git-gamble`

* `git-gamble` {{#include ./_logo_mini_front.html}}
  is a tool that helps to develop the right thing 😌,
  [baby step by baby step 👶🦶](./slides_why/)
* `git-gamble` {{#include ./_logo_mini_front.html}}
  use [the TCRDD method](./theory.md)
* `git-gamble` {{#include ./_logo_mini_front.html}}
  is a [`git` addon](https://github.com/stevemao/awesome-git-addons#readme)

[Install](./install/index.html) the tool

Read about [the usage](./usage/index.html)

Watch [the demo](./usage/demo/index.html)

[![asciicast](https://asciinema.org/a/496959.svg)](./usage/demo/index.html)
