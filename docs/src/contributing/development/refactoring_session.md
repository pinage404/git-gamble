# Refactoring session

At the beginning of your session, just run the following command :

```sh
watchexec -- git gamble --refactor
```

This is just [TCR](../../theory.md#tcr) (without [TDD](../../theory.md#tdd))
