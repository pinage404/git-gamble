# Debug

There are some logs in the programs, [`pretty_env_logger`](https://docs.rs/pretty_env_logger) is used to display them

There are 5 levels of logging (from the lightest to the most verbose) :

- `error`
- `warn`
- `info`
- `debug`
- `trace`

The `git-gamble` logs are "hidden" behind the `with_log` [feature](https://doc.rust-lang.org/cargo/reference/features.html)

The option `--features with_log` (or `--all-features`) must be added to each `cargo` command for which you want to see logs (e.g.) :

```sh
cargo build --all-features
cargo run --all-features
cargo test --all-features
```

Then, to display logs, add this environment variable :

```sh
export RUST_LOG="git_gamble=debug"
```

To display _really_ **everything** :

```sh
export RUST_LOG="trace"
```

There are other possibilities for logging in the [`env_logger` documentation](https://docs.rs/env_logger/)

You can also uncomment [variables in the setup script](https://gitlab.com/pinage404/git-gamble/-/blob/d193b7019dcab6f343934d9e44a3ba9dc29af26f/script/setup_variables.sh#L12)

There is also a command that executes a test with all the outputs displayed

```sh
mask test rust debug <the test to filter>
```

Exemple

```sh
mask test rust debug commit_when_tests_fail
```
