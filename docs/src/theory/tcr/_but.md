But it **doesn't** allow us to **see** the **tests failing**

So:

<v-clicks>

- Maybe we test nothing (assert forgotten)

  ```python
  def test_should_be_Buzz_given_5():
      input = 5
      actual = fizz_buzz(input)
      # Oops! Assert has been forgotten
  ```

- Maybe we are testing something that is not the thing we should be testing

  ```typescript
  it("should be Fizz given 3", () => {
    const input = 3;
    const actual = fizzBuzz(input);
    expect(input).toBe("Fizz");
    // Oops! Asserts on the input value instead of the actual value
  });
  ```

</v-clicks>
