#!/usr/bin/env sh

set -o errexit
set -o nounset

GAMBLED="$1"
ACTUAL="$2"

if [ "pass" = "${GAMBLED}" ] && [ "pass" = "${ACTUAL}" ]; then
    git pull --rebase
    eval "${GAMBLE_TEST_COMMAND}"
    git push
fi
