#!/usr/bin/env sh
set -o errexit -o nounset -o xtrace

PACKAGE="$1"
BUILD="result/$1"
IMAGE="$2"

nix build \
	--out-link "$BUILD" \
	".#$PACKAGE"

"$BUILD" |
	pigz |
		skopeo copy \
			docker-archive:/dev/stdin \
			"docker://$IMAGE"
