# Commands

## test

```bash
set -o errexit -o nounset -o pipefail -o errtrace

$MASK test shell
$MASK test rust
```

### test shell

```bash
set -o errexit -o nounset -o pipefail -o errtrace

./tests/test_scripts.sh
./script/tests/test_generate_completion.sh
```

### test rust

```sh
cargo test --features with_log
```

#### test rust with_coverage

```sh
cargo tarpaulin --engine Llvm  --locked --out Xml --features with_log
```

#### test rust debug (FILTERS)

```sh
export RUST_LOG="trace"
export RUST_TRACE="full"
# export RUST_BACKTRACE="full"
export RUST_TEST_NOCAPTURE="display stdout and stderr"

cargo test \
    --features with_log \
    -- \
    "$FILTERS"
```

### test watch

```sh
bacon test --features with_log
```

### test mutants

```sh
cargo mutants
```

## lint

```bash
set -o errexit -o nounset -o pipefail -o errtrace

cargo clippy --all-targets --all-features
cargo check --all-targets --all-features
masklint run
```

## format

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix fmt ./**.nix
cargo fix --all-features --allow-dirty --allow-staged
cargo fmt
```

## docs

### docs serve

```bash
pushd ./docs/
direnv exec . \
    mask docs serve
```

## slides

### slides serve (SLIDE_FILE)

```bash
pushd ./slides/
direnv exec . \
    mask slides serve $SLIDE_FILE
```

## update

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake update
direnv exec . \
    cargo update
pushd ./slides
direnv exec . \
    corepack use pnpm@latest
direnv exec . \
    pnpm update --latest
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
