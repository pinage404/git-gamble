#!/usr/bin/env bash

# default `git-gamble`'s command test the code
export GAMBLE_TEST_COMMAND="mask test"

# export TIME_KEEPER_MAXIMUM_ITERATION_DURATION=$((60 * 3))

# debug
# export RUST_LOG="trace"
# export RUST_TRACE="full"
# export RUST_BACKTRACE="1"
# export RUST_BACKTRACE="full"
# export RUST_TEST_NOCAPTURE="display stdout and stderr"
