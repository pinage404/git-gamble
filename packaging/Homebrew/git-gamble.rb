class GitGamble < Formula
  desc "Tool that blends TDD (Test Driven Development) + TCR (test && commit || revert)"
  homepage "https://git-gamble.is-cool.dev"
  license "ISC"
  head "https://gitlab.com/pinage404/git-gamble.git", branch: "main"

  depends_on "rust" => :build

  def install
    system "cargo", "install", *std_cargo_args

    generate_completions_from_executable(bin/"git-gamble", "generate-shell-completions")
  end

  test do
    system "git", "init"
    system "git", "config", "user.name", "Git Gamble"
    system "git", "config", "user.email", "git@gamble"

    system "sh", "-c", "echo 'failing' > 'some_file'"
    system bin/"git-gamble", "--red", "--", "false"
    assert_equal "failing", shell_output("cat some_file").strip

    system "sh", "-c", "echo 'should pass but still failing' > 'some_file'"
    system bin/"git-gamble", "--green", "--", "false"
    assert_equal "failing", shell_output("cat some_file").strip

    system "sh", "-c", "echo 'passing' > 'some_file'"
    system bin/"git-gamble", "--refactor", "--", "true"
    assert_equal "passing", shell_output("cat some_file").strip
  end
end
