use std::env;
use std::io;
use std::path::PathBuf;
use test_case::test_case;

use crate::fixture::TestRepository;

#[test_log::test]
fn command_can_have_arguments() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	test_repository
		.gamble_with(&[
			"--pass",
			"--",
			"sh",
			"-c",
			"test 'hello world' = 'hello world'",
		])
		.success();

	assert!(test_repository.is_clean());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_new_commit());
	Ok(())
}

#[test_case("--pass", "tests/fixture/test_pass.sh")]
#[test_case("--fail", "tests/fixture/test_fail.sh")]
#[test_log::test]
fn command_can_be_a_script(gambling_flag: &str, script: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let script_path = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join(script);
	let script = script_path.to_str().unwrap();

	test_repository
		.gamble_with(&[gambling_flag, "--", "sh", script])
		.success();

	assert!(test_repository.is_clean());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_new_commit());
	Ok(())
}
