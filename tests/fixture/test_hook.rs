use speculoos::assert_that;
use speculoos::path::PathAssertions;
use speculoos::prelude::ResultAssertions;
use speculoos::prelude::StrAssertions;
use std::fs::read_to_string;
use std::io;
use std::path::PathBuf;
use tempfile::TempDir;
use tempfile::tempdir;

use crate::fixture::TestRepository;

pub(crate) struct TestHook {
	// tempdir will remove directory when last reference is freed, keep it in the struct to keep the directory
	#[allow(dead_code)]
	output_directory: TempDir,

	pub(crate) output_path: PathBuf,
}

impl TestHook {
	pub(crate) fn init(test_repository: &TestRepository, hook_name: &str) -> io::Result<TestHook> {
		let hook_output_directory = tempdir()?;
		let hook_output_path = hook_output_directory.path().join("hook_output");

		test_repository.add_hook(
			hook_name,
			format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
		)?;

		Ok(TestHook {
			output_directory: hook_output_directory,
			output_path: hook_output_path,
		})
	}

	pub(crate) fn assert_that_hook_has_been_run(&self) {
		assert_that(&self.output_path).exists();
	}

	pub(crate) fn assert_that_hook_has_not_been_run(&self) {
		assert_that(&self.output_path).does_not_exist();
	}

	pub(crate) fn assert_that_hook_has_been_run_with(&self, content: &str) {
		let hook_output = read_to_string(&self.output_path);
		assert_that(&hook_output).is_ok().contains(content);
	}
}
