#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")

FILE_TO_EDIT=""

oneTimeSetUp() {
    FILE_TO_EDIT="${SHUNIT_TMPDIR}/file_to_edit"
}

tearDown() {
    rm --force "${FILE_TO_EDIT}"
}

test_fake_editor_exit_without_errors() {
    EDITOR="${DIRECTORY_PATH}/fake_editor.sh 'ignored file content'"

    eval "${EDITOR}" "${FILE_TO_EDIT}"

    assertEquals "exit without errors" 0 $?
}

test_fake_editor_set_content_with_the_first_argument() {
    EXPECTED_CONTENT='expected message'
    EDITOR="${DIRECTORY_PATH}/fake_editor.sh '${EXPECTED_CONTENT}'"

    eval "${EDITOR}" "${FILE_TO_EDIT}"

    ACTUAL_CONTENT=$(cat "${FILE_TO_EDIT}")
    assertEquals "file contents are different" "${EXPECTED_CONTENT}" "${ACTUAL_CONTENT}"
}

test_fake_editor_replace_existing_content() {
    echo 'initial content' >"${FILE_TO_EDIT}"

    EXPECTED_CONTENT='expected message'
    EDITOR="${DIRECTORY_PATH}/fake_editor.sh '${EXPECTED_CONTENT}'"

    eval "${EDITOR}" "${FILE_TO_EDIT}"

    ACTUAL_CONTENT=$(cat "${FILE_TO_EDIT}")
    assertEquals "file contents are different" "${EXPECTED_CONTENT}" "${ACTUAL_CONTENT}"
}

. shunit2
