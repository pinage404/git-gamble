import { defineMonacoSetup } from '@slidev/types'

export default defineMonacoSetup(() => ({
    editorOptions: {
        lineNumbers: 'on',
        renderLineHighlight: 'none',
        renderSideBySide: false,
        renderGutterMenu: false,
    },
}))
