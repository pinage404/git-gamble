---
favicon: '/logo.svg'
addons:
  - slidev-component-zoom
selectable: true
---

# test components

<div><Red /></div>

<div><Green /></div>

<div><Refactor /></div>

<div><Command>git-gamble --help</Command></div>

<div><TossACoin result="committed" /><TossACoin result="reverted" /></div>

<div><Committed /></div>

<div><Reverted /></div>

<QuickSave />

<Loader>💾</Loader>

---
layout: component-demo
---

<Character />

<Character state="falling" />

<Character state="dead" />

---

<img src="/boulder.svg" class="boulder" alt="" />

<style>
img {
  max-width: 100%;
  max-height: 100%;
}
</style>
