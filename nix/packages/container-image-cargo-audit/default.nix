{
  pkgs,
}:

pkgs.dockerTools.streamLayeredImage {
  name = "container-image-cargo-audit";
  contents = [
    pkgs.dockerTools.binSh
    pkgs.dockerTools.caCertificates
    pkgs.busybox
    pkgs.cargo-audit
  ];
}
