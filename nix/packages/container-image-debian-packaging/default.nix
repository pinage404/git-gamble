{
  inputs,
  pkgs,
}:

pkgs.dockerTools.streamLayeredImage {
  name = "container-image-debian-packaging";
  fromImage = inputs.self.packages."${pkgs.system}".container-image-rust-built;
  contents = [
    pkgs.dockerTools.usrBinEnv
    pkgs.cargo-deb
  ];

  extraCommands = ''
    mkdir lib64
    ln --symbolic "${pkgs.stdenv.cc.bintools.dynamicLinker}" lib64/ld-linux-x86-64.so.2
  '';
}
