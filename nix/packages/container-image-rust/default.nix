{
  lib,
  pkgs,
}:

let
  rust-toolchain = lib.internal.rust-toolchain-minimal pkgs;
in
pkgs.dockerTools.streamLayeredImage {
  name = "container-image-rust";
  contents = [
    pkgs.dockerTools.binSh
    pkgs.dockerTools.caCertificates
    rust-toolchain
    pkgs.stdenv.cc
    pkgs.busybox
  ];

  extraCommands = ''
    mkdir tmp
  '';
}
