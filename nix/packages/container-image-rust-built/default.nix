{
  inputs,
  pkgs,
}:

let
  stream = inputs.self.packages."${pkgs.system}".container-image-rust;
in
pkgs.runCommand "${stream.imageName}.tar.gz" {
  inherit (stream) imageName;
  passthru = {
    inherit (stream) imageTag;
    inherit stream;
  };
  nativeBuildInputs = [ pkgs.pigz ];
} "${stream} | pigz > $out"
