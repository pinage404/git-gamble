{
  lib,
  pkgs,
}:

let
  rust-toolchain = lib.internal.rust-toolchain-with {
    inherit pkgs;
    toolchainOverride = {
      components = [ ];
      profile = "minimal";
      targets = [
        "x86_64-pc-windows-gnu"
      ];
    };
  };
in
pkgs.dockerTools.streamLayeredImage {
  name = "container-image-rust-windows";
  contents = [
    pkgs.dockerTools.binSh
    pkgs.dockerTools.caCertificates
    rust-toolchain
    pkgs.stdenv.cc
    (pkgs.pkgsCross.mingwW64.stdenv.cc.override ({
      extraBuildCommands = ''
        printf '%s' '-L ${pkgs.pkgsCross.mingwW64.windows.mingw_w64_pthreads}/lib' >>$out/nix-support/cc-ldflags
      '';
    }))
    pkgs.busybox
  ];

  extraCommands = ''
    mkdir tmp
  '';
}
