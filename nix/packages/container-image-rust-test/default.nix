{
  inputs,
  pkgs,
}:

pkgs.dockerTools.streamLayeredImage {
  name = "container-image-rust-test";
  fromImage = inputs.self.packages."${pkgs.system}".container-image-rust-built;
  contents = [
    pkgs.dockerTools.usrBinEnv
    pkgs.gitMinimal
    pkgs.cargo-tarpaulin
  ];

  enableFakechroot = true;
  fakeRootCommands = ''
    ${pkgs.dockerTools.shadowSetup}
    useradd normal_user
  '';
}
