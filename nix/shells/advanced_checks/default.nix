{
  pkgs,
}:

pkgs.mkShellNoCC {
  packages = [
    # needed only to check crates vulnerabilities
    pkgs.cargo-audit
    # needed only to detect what can be improved
    pkgs.cargo-bloat
    pkgs.cargo-diet
    pkgs.cargo-license
  ];
}
