{
  pkgs,
}:

pkgs.mkShellNoCC {
  packages = [
    pkgs.nodejs
    pkgs.corepack
    pkgs.mask
  ];
}
