{
  pkgs,
}:

let
  add_skopeo_policy = pkgs.writeShellScriptBin "add_skopeo_policy" ''
    mkdir --parents "/etc/containers"
    ln --symbolic "${pkgs.skopeo.policy}/default-policy.json" "/etc/containers/policy.json"
  '';
in
pkgs.mkShellNoCC {
  packages = [
    pkgs.concurrently
    pkgs.pigz
    pkgs.skopeo
    add_skopeo_policy
  ];
}
