{
  pkgs,
}:

[
  pkgs.internal.mdbook
  pkgs.mdbook-mermaid
  pkgs.mdbook-yml-header
]
